from ku import ku, tcpsession, Reject
import logging

from cubelib import state, NextState, bound
import cubelib
from types import ModuleType
from importlib import import_module
import traceback

version = '0.1.0'

class E:
    #???
    map = []

def protoconverter(packet, target_proto):
    return packet

class myrror(object):

    alive: bool = True

    class MinecraftSession(tcpsession):

        state: state = state.Handshaking        
        compression: int = -1

        ServerBoundBuffer: list
        ClientBoundBuffer: list

        inproto: ModuleType
        outproto: ModuleType
        
        def __init__(self, client, server, proxy, protomod):
            
            self.client = client
            self.server = server
            self.proxy = proxy
            self.id = id(self)
                        
            self.outproto = protomod

            self.ServerBoundBuff = [b""] # it's a little trick to make immutable type (bytes)
            self.ClientBoundBuff = [b""] # mutable to pass reference to it

            print(F"#{self.id} new conn {client.getpeername()}->{client.getsockname()}::{server.getsockname()}->{server.getpeername()}")

        def clientbound(self, data):        
            return self._handle_bytes(data, self.ClientBoundBuff, cubelib.bound.Client)
    
        def serverbound(self, data):        
            return self._handle_bytes(data, self.ServerBoundBuff, cubelib.bound.Server)

        def connection_made(self):
            print(F"#{self.id} connection_made")
    
        def connection_lost(self, side, err):
            side = 'client' if side is self.client else 'server' if side is not None else 'proxy'
            print(F"#{self.id} connection_lost by {side} due to {err}")
        
        def _handle_bytes(self, data, buff, bound):
            try:
                packs = []
                buff[0] += data            
                buff[0] = cubelib.readPacketsStream(buff[0], self.compression, bound, packs)
                
                ret = b""
                for p in packs:
                    handler = self._handle_packet_cbound if bound is bound.Client else self._handle_packet_sbound
                    hr = handler(p)
                    if hr is Reject:
                        return Reject
                    ret += hr

            except Exception as e:
                print(f"Exception in {bound.name}Bound: {e}")
                print(traceback.format_exc())

            return ret if len(ret) > 0 else Reject
        
        def _handle_packet_cbound(self, pack):

            proto = cubelib.proto if self.state in [state.Handshaking, state.Status] else self.outproto                
            try:
                p = pack.resolve(self.state, proto)
            except Exception:
                return pack.build(self.compression)
            t = p.__class__

            if self.state is state.Login:
                # Handle SetCompression
                if t is proto.ClientBound.Login.SetCompression:
                    self.compression = p.Threshold
                # Handle LoginSuccess
                if t is proto.ClientBound.Login.LoginSuccess:
                    self.state = cubelib.state.Play
                
                return pack.build(self.compression if pack.compressed else -1)

            elif self.state is state.Play:
                return protoconverter(pack, self.inproto, self.compression)
            
            else:
                return pack.build()
        
        def _handle_packet_sbound(self, pack):
            
            proto = cubelib.proto if self.state in [state.Handshaking, state.Status] else self.inproto
            try:
                p = pack.resolve(self.state, proto)
            except Exception:
                return pack.build(self.compression)
            t = p.__class__

            if t is cubelib.proto.ServerBound.Handshaking.Handshake:
                # Handle Handshake
                return self._handle_handshake(p)

            if self.state is not state.Play:
                return pack.build(self.compression if pack.compressed else -1)

            elif self.state is state.Play:
                return protoconverter(pack, self.outproto, self.compression)

        def _handle_handshake(self, p):

            if p.NextState == cubelib.NextState.Status:
                self.state = state.Status
                return p.build()     

            self.state = state.Login
            if p.ProtoVer not in cubelib.supported_versions:                
                self.client.sendall(cubelib.proto.ClientBound.ClassicLogin.Disconnect(f"proxy.have.not.support.protocol.v{p.ProtoVer}.yet").build())
                self.proxy.terminate(self)
                return Reject      

            self.inproto = import_module(f"cubelib.proto.v{p.ProtoVer}")
            p = cubelib.proto.ServerBound.Handshaking.Handshake(self.outproto.version, p.ServerName, p.ServerPort, p.NextState)
            return p.build()
            
    def __init__(self, listen: tuple, upstream: tuple, protocol: int, verbose: bool = False):

        self.logger = logging.getLogger("MYRROR")
        self.logger.setLevel(logging.DEBUG if verbose else logging.ERROR)
        self.logger.info(F"Running MYRROR v{version}")
        self.logger.info(F"Proxying config is: \u001b[97m{listen[0]}:{listen[1]} \u001b[92m-> \u001b[97m{upstream[0]}:{upstream[1]}")
        if protocol not in cubelib.supported_versions:
            self.logger.critical(F"Selected outgoing protocol is not supported by cubelib! Supported: {cubelib.supported_versions}")
            raise RuntimeError()            

        protocol_mod = import_module(f"cubelib.proto.v{protocol}")
        self.logger.info(F"Selected outgoing protocol: {protocol}")
        self.logger.debug("Starting proxy...")
        self.proxy = ku(listen, upstream, self.MinecraftSession, [protocol_mod], maxcon=1)
    
    def shutdown(self):

        self.proxy.shutdown()